import React, {useEffect, useState} from "react";
import Sidebar from './Sidebar'
import Chat from './Chat'
import "./App.css";
import Pusher from 'pusher-js'
import Axios from './Axios'

function App() {
  const [messages, setmessages] = useState([])

  useEffect(() => {
    Axios.get('/messages/sync').then((response) =>{
      setmessages(response.data)
    })
  }, [])

  useEffect(() => {
    var pusher = new Pusher('581518a91c4b25e97c3d', {
      cluster: 'ap2'
    });

    const channel = pusher.subscribe('messages');
    channel.bind('inserted', (newMessages)=> {
      setmessages([...messages, newMessages])
    });

    return()=>{
      channel.unbind_all();
      channel.unsubscribe();
    }
  }, [messages])

  console.log(messages)
  return (
    // BEM Naming Convention
    <div className="app">
      <div className="app__body">
        <Sidebar/>
        <Chat messages={messages}/>
      </div>
    </div>
  );
}

export default App;
