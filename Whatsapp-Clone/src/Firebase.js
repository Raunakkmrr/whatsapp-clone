import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBAze_GNXxlNhAPynC9IQ1gn6-o7QM9KRE",
  authDomain: "whatsapp-c20b7.firebaseapp.com",
  databaseURL: "https://whatsapp-c20b7.firebaseio.com",
  projectId: "whatsapp-c20b7",
  storageBucket: "whatsapp-c20b7.appspot.com",
  messagingSenderId: "1023181967239",
  appId: "1:1023181967239:web:baf10d60f0943412454d22",
  measurementId: "G-GW1N50C35V"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();
export { auth, provider };
export default db;
