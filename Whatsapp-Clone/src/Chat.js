import React, { useState, useEffect } from "react";
import { Avatar, IconButton } from "@material-ui/core";
import MicIcon from "@material-ui/icons/Mic";
import {
  SearchOutlined,
  MoreVert,
  AttachFile,
  InsertEmoticon,
} from "@material-ui/icons";
import "./chat.css";
import axios from './Axios'

function Chat({ messages }) {
  const [seed, setseed] = useState("");
  const [Input, setInput] = useState("");
  useEffect(() => {
    setseed(Math.floor(Math.random() * 5000));
  }, []);

  const sendMessage =  async(e) => {
    e.preventDefault();
    console.log("you typed >>> " + Input);
    await axios.post('/messages/new', {
      "message":Input,
      "name":"Do authentication and set the name here ",
      "timestamp":"Timestamp to be done",
      "received":false
    })
    setInput("");
  };
  return (
    <div className="chat">
      <div className="chat__header">
        <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`} />
        <div className="chat__headerInfo">
          <h2>room Name</h2>
          <p>Last Seen</p>
        </div>
        <div className="chat__headerRight">
          <IconButton>
            <SearchOutlined />
          </IconButton>
          <IconButton>
            <AttachFile />
          </IconButton>
          <IconButton>
            <MoreVert />
          </IconButton>
        </div>
      </div>
      <div className="chat__body ">
        {messages.map((message) => (
          <p
            className={`chat__message ${!message.received && "chat__reciever"}`}
          >
            <span className="chat__name">{message.name}</span>
            {message.message}
            <span className="chat__timeStamp">{message.timestamp}</span>
          </p>
        ))}
      </div>
      <div className="chat__footer">
        <IconButton>
          <InsertEmoticon style={{ marginRight: -15 }} />
        </IconButton>
        <form>
          <input
            value={Input}
            onChange={(e) => {
              setInput(e.target.value);
            }}
            type="text"
            placeholder="Type a message"
          />
          <button type="submit" onClick={sendMessage}>
            Send a Message
          </button>
        </form>
        <IconButton>
          <MicIcon style={{ marginLeft: -15 }} />
        </IconButton>
      </div>
    </div>
  );
}

export default Chat;
