import React, {useState, useEffect} from "react";
import { Avatar } from "@material-ui/core";
import "./sidebarchat.css";

function SidebarChat({addChat, id, name}) {
    const [seed, setseed] = useState('')

    useEffect(() => {
        setseed(Math.floor(Math.random()*5000))
    }, [])

    const createChat = ()=>{
        const newChat = prompt("Enter the name of New Chat")

        if(newChat){
            // do something afterwards
        }
    }
  return !addChat ?(
    <div className="sidebarChat">
      <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`}/>
      <div className="sidebarChat__info">
        <h2>{name}</h2>
        <p>Last message...</p>
      </div>
    </div>
  ):(
    <div className="sidebarChat" onClick={createChat}>
        <h2>Add New Chat</h2>
      </div>
  );
}

export default SidebarChat;
