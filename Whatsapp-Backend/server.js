//importing
var express = require("express");
var mongoose = require("mongoose");
var Messages = require("../Whatsapp-backend/dbMessages.js");
var Pusher = require("pusher");
var cors = require("cors");
require('dotenv').config();

//app config
const app = express();
const port = process.env.PORT;

var pusher = new Pusher({
  appId: "1071517",
  key: "581518a91c4b25e97c3d",
  secret: "21d99c1a5a20b80d9cb4",
  cluster: "ap2",
  encrypted: true,
});

//middleware----> so what happens that you can run the site on postman, you'll even get the id,
//but not in the proper format..... you want that in json. Ao if you forget this step you'll not get the data in proper format
app.use(express.json());
app.use(cors());
// app.use((req, res, next)=>{
//   res.setHeader('Access-Control-Allow-Origin', "*")
//   res.setHeader('Access-Control-Allow-Headers', "*")
//   next();
// })
//DB config
const connection_URL = process.env.MONGO_URI;

mongoose.connect(connection_URL, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.once("open", () => {
  console.log("db connected");
  const msgCollection = db.collection("messagecontents");
  const changeStream = msgCollection.watch();

  changeStream.on("change", (change) => {
    console.log(change);

    if (change.operationType === "insert") {
      const messageDetails = change.fullDocument;
      pusher.trigger("messages", "inserted", {
        name: messageDetails.name,
        message: messageDetails.message,
        timestamp: messageDetails.timestamp,
        received: messageDetails.received,
      });
    }
  });
});
// ??????

// api routes
app.get("/", (req, res) => {
  res.status(200).send("hello world");
});

app.get("/messages/sync", (req, res) => {
  Messages.find((err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
});

app.post("/messages/new", (req, res) => {
  const dbMessage = req.body;

  Messages.create(dbMessage, (err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(201).send(data);
    }
  });
});

//listen
app.listen(port, () => console.log(`Listening on localhost:${port}`));
